<?php
/**
 * @file
 * Tag engine
 * Finds and replaces hexes based on a tags in CSS
 * Not coincidentally, the tags replaced are the field names. :)
 */

function _color_engine_tag($style, $info, $palette, $options = NULL) {
  _color_switch('', $palette);
  $style = preg_replace_callback("~(?:/\*\s*\{\{\s*([^/*]+)\s*\}\}\s*\*/)?\s*(#[0-9a-f]{3,6})\s*(?:/\*\s*\{\{\s*\/?\s*([^/*]+)\s*\}\}\s*\*/)?~is", "_color_switch", $style);
 
  return $style;
}

/**
 * Find if tag matches with palette field. If so, return corresponding hex.
 * Intended to return results to the preg_replace_callback() in _color_engine_tag()
 *
 * @param $matches
 *  Array of regex matches
 * @param $static
 *  Array of field names + hexes for scheme (palette)
 * @return
 *  Return an array of hexes
 *  or the same code it gave if no match found.
 */
function _color_switch($matches, $static = FALSE) {
  static $palette;

  if ($static != FALSE) {
    $palette = $static;
  }
  else {
    // See if any of the regexes match
    if ($match = array_intersect(array_keys($palette), array_values($matches))) {
      $matches = $palette[array_shift(array_values($match))];
    }
    else {
      $matches = $matches[0];
    }
  }
  return $matches;
}