<?php
/**
 * @file
 * Database magician functions
 */
 
/**
 * Retrieve schemes through various methods
 * @param $method: can be set to 'scheme_id', 'hex', 'uid', 'theme' for default scheme
 * @param $vars: the ID, unserialized hex sep. by comma, uid, theme name.
 *        'hex' and 'scheme_name' require array('hex' => $hex, 'theme' => $theme), so on.
 * @param $select: can be used to retrieve only selected columns
 */
function color_get_scheme($method, $vars, $columns = NULL) {
  static $static = array();

  $vars_serialized = (is_array($vars)) ? implode(', ', $vars) : $vars;
  if (isset($columns)) {
    $select = (is_array($columns)) ? implode(', ', $columns) : $columns;
  }
  else {
    $select = '*';
  }
  
  if (isset($static[$method][$vars_serialized][$select])) {
    return $static[$method][$vars_serialized][$select];
  }

  // Switch between our scheme methods
  switch ($method):
  
  case 'scheme_id':
    $result = db_query("SELECT %s FROM {color_schemes} cs ".
    "WHERE cs.id = '%s'", $select, $vars);
    break;
  case 'uid':
    $result = db_query("SELECT %s FROM {color_users} cu ".
    "RIGHT JOIN {color_schemes} cs ON cu.scheme_id = cs.id ".
    "WHERE cu.uid = '%s'", $select, $vars);
    break;
  case 'hex':
    $result = db_query('SELECT '.$select.' FROM {color_schemes} cs '.
    'WHERE cs.theme = "'.$vars['theme'].'" AND cs.hex = \''.$vars['hex'].'\'');
    break;
  case 'scheme_name':
    $result = db_query('SELECT '.$select.' FROM {color_schemes} cs '.
    'WHERE cs.theme = "'.$vars['theme'].'" AND cs.name = \''.$vars['scheme_name'].'\'');
    break;
  case 'theme':
    $result = db_query("SELECT %s FROM {color_schemes} cs ".
    "WHERE cs.id IN ".
    "(SELECT c.value FROM {color} c WHERE ".
    "c.name = 'default_scheme' and c.theme = '%s')", $select, $vars);
    break;
  endswitch;
  
  if ($rows = db_fetch_array($result)) {
    $static[$method][$vars_serialized][$select] = $rows;
    $scheme = TRUE;
  }
  
  
  return $scheme ? $rows : FALSE;
}

/**
 * Set UID or anonymous user to a scheme
 * @param $scheme_id
 *   Integer of scheme ID.
 * @param $vars
 *   Optional array. Scheme info to update or add.
 * @param $method
 *   Optional string. 'update', 'delete', 'insert'
 */
function color_set_scheme($scheme_id, $vars = NULL, $method = NULL) {
  if (!isset($method)) {
    $results = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {color_schemes} WHERE id = '%s'", $scheme_id));
    if ($results['count'] > 0) {
      $method = 'update';
    }
    else {
      $method = 'insert';
    }
  }

  if ($method == 'insert' && isset($vars)) {
    db_query('INSERT INTO {color_schemes} (name, theme, hex, status) VALUES (\'%s\', \'%s\', \'%s\', \'%s\')', $vars['name'], $vars['theme'], serialize($vars['hex']), '2');
    $last_id = db_last_insert_id('color_schemes', 'id');
    color_add_scheme($vars['theme'], $vars['hex'], $last_id, $vars['name']);
    drupal_set_message("Scheme <em>".$vars['name']."</em> added.", "info");
    return $last_id;
  }
  elseif ($method == 'update') {
    db_query('UPDATE {color_schemes} SET extra_attr = \'%s\', hex = \'%s\', name = \'%s\' WHERE id = \'%s\'', serialize($vars['extra_attr']), serialize($vars['hex']), $vars['schemename'], $scheme_id);
  }
  elseif ($method == 'delete' && isset($scheme_id)) {
    db_query('DELETE FROM {color_schemes} WHERE id = \'%s\'', $scheme_id);
    drupal_set_message('Scheme deleted.', "info");
    return "deleted";
  }
  
}

/**
 * Set UID or anonymous user to a scheme
 * @param $scheme_id: the scheme ID
 * @param $uid: User's ID
 * @param $method: 'update', 'delete', 'insert'
 */
function color_set_user_scheme($scheme_id, $uid = 0, $method = NULL) {
  if (!isset($method)) {
    $results = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {color_users} WHERE uid = '%s'", $uid));
    if ($results['count'] > 0) {
      $method = 'update';
    }
    else {
      $method = 'insert';
    }
  }

  if ($uid == 0) {
    if ($method == 'delete') {
      unset($_SESSION['scheme_id']);
    }
    else {
      $_SESSION['scheme_id'] = $scheme_id;
    }
  }
  elseif (isset($scheme_id) && $uid != 0) {
    if ($method == 'update') {
      db_query('UPDATE {color_users} SET scheme_id = \'%s\' WHERE uid = \'%s\'', $scheme_id, $uid);
      drupal_set_message(t('Your color scheme selection has been modified in the DB.'), "info");
    }
    elseif ($method == 'insert') {
      db_query('INSERT INTO {color_users} (uid, scheme_id) VALUES (\'%s\', \'%s\')', $uid, $scheme_id);
      drupal_set_message('Your color scheme selection has been added to the DB.', "info");
    }
    elseif ($method == 'delete') {
      db_query('DELETE FROM {color_users} WHERE uid = \'%s\'', $uid);
      drupal_set_message('Your color scheme selection has been removed.', "info");
    }
  }
}

/**
 * Check if scheme_id matches up with a real scheme
 * @param $scheme_id: the scheme ID
 * @param $theme: the theme name, used if trying to match scheme_id to theme
 */
function color_scheme_id_valid($scheme_id, $theme = NULL) {
  static $static;
  
  if (!isset($theme) && isset($static[$scheme_id])) {
    return $static[$scheme_id];
  }
  elseif (isset($theme) && isset($static[$scheme_id][$theme])) {
    return $static[$scheme_id][$theme];
  }
  
  if ($scheme = color_get_scheme('scheme_id', $scheme_id, 'theme')) {
    if ((isset($theme)) && ($theme != $scheme['theme'])) {
      $static[$scheme_id][$theme] = FALSE;
      return $static[$scheme_id][$theme];
    }
    else {
      $static[$scheme_id][$theme] = TRUE;
      return $static[$scheme_id][$theme];
    }
  }
  else {
    $static[$scheme_id] = FALSE;
    return FALSE;
  }
}

/**
 * Is system info for theme generated in {color}?
 * Returns TRUE or FALSE
 */
function color_theme_info_generated($theme) {
  static $static;
  
  if (isset($static[$theme])) {
    return $static[$theme];
  }
  
  $result = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {color} WHERE theme = '%s'", $theme));
  $rows = $result['count'];
  
  if ($rows == 0) {
    $static[$theme] = FALSE;
  }
  else {
    $static[$theme] = TRUE;
  }
  
  return $static[$theme];
}

/**
 * Are default schemes in color.inc generated for theme?
 * Returns TRUE or FALSE.
 */
function color_theme_scheme_generated($theme) {
  static $static;
  
  if (isset($static[$theme])) {
    return $static[$theme];
  }
  
  $result = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {color_schemes} WHERE theme = '%s'", $theme));
  $schemes = $result['count'];
  
  if ($schemes == 0) {
    $static[$theme] = FALSE;
  }
  else {
    $static[$theme] = TRUE;
  }
  
  return $static[$theme];
}

/**
 * Check if color info for color themes are installed in DB and generated.
 * Used in color.install for all themes and in admin/build/themes/settings/{theme}
 * Alternate use: check to see if schemes are generated. If not, returns false.
 * @param $theme: Theme name
 */
function color_check_themes($theme = '') {
  static $checked;

  module_load_include('inc', 'color', 'color.misc');

  if (isset($checked) && $checked == TRUE) {
    return;
  }
  
  // Check to see if colortheme are in DB for theme yet
  if (empty($theme)) {
    $themes = color_list_colorable_themes();
  }
  else {
    $themes[] = $theme;
  }
  
  module_load_include('module', 'color', 'color');

  foreach ($themes as $theme) {  
    $theme_generated = color_theme_info_generated($theme);
    $scheme_generated = color_theme_scheme_generated($theme);

    if ($theme_generated === FALSE || $scheme_generated === FALSE) {
      $info = color_get_theme_info($theme, 'inc');

      if (!color_validate($info)) {
        return;
      }
      else {
        if ($theme_generated === FALSE) {
          color_add_theme($theme);
        }
        if ($scheme_generated === FALSE) {
          color_add_schemes($theme);
        }
      }
    }

  }
  
  $checked = TRUE;
}

/**
 * Add color theme info to database
 * @param $theme: Theme name
 */
function color_add_theme($theme) {
  module_load_include('module', 'color', 'color');
  $info = color_get_theme_info($theme, "inc");
  
  // Add fields to db
  db_query('INSERT INTO {color} (name, theme, value) VALUES (\'%s\', \'%s\', \'%s\')', "fields", $theme, serialize($info['fields']));
  
  // Add other info to db
  $otherkeys = array('engine', 'css', 'copy', 'blend_target', 'preview', 'img');
  foreach ($otherkeys as $key) {
    if (isset($info[$key])) {
      $storage[$key] = $info[$key];
      db_query('INSERT INTO {color} (name, theme, value) VALUES (\'%s\', \'%s\', \'%s\')', $key, $theme, serialize($storage[$key]));
    }
  }
  
  db_query('INSERT INTO {color} (name, theme, value) VALUES (\'%s\', \'%s\', \'%s\')', 'default_scheme', $theme, $info['default_scheme']);

  db_query('INSERT INTO {color} (name, theme, value) VALUES (\'%s\', \'%s\', \'%s\')', 'reference_scheme', $theme, serialize($info['schemes'][$info['reference_scheme']]));
  
  drupal_set_message("Added information for <em>{$theme}</em> to color database", "status");
}

/**
 * Add pre-made themes into DB
 * @param $theme: Theme name
 */
function color_add_schemes($theme) {
  module_load_include('module', 'color', 'color');
  $info = color_get_theme_info($theme, "inc");
  
  foreach ($info['schemes'] as $name => $hex) {
    
    // Add colors to database  
    db_query('INSERT INTO {color_schemes} (name, theme, hex, status) VALUES (\'%s\', \'%s\', \'%s\', \'%s\')', $name, $theme, serialize($hex), '2');
    $last_id = db_last_insert_id('color_schemes', 'id');
//        drupal_set_message($info['default_scheme']. " == {$name} == {$last_id}");

    if (strstr($info['default_scheme'], $name)) {
      db_query('UPDATE {color} SET value = \'%s\' WHERE theme = \'%s\' AND name = \'default_scheme\'', $last_id, $theme);
    }
    else {
      $result = db_fetch_array(db_query("SELECT COUNT(*) AS count FROM {color} WHERE theme = '%s' AND name = 'default_scheme'", $theme));
      $count = $result['count'];
    
      if ($count < 1) {
        db_query('INSERT INTO {color} (name, theme, value) VALUES (\'%s\', \'%s\', \'%s\')', "default_scheme", $theme, $last_id);
      }
    }

    $schemes[] = array(
      'theme' => $theme,
      'hex' => $hex,
      'last_id' => $last_id,
      'name' => $name,
      'palette' => '',
    );
    
    //$theme, $schemehex, $scheme_id, $palette = ''
  }
  foreach ($schemes as $value) {
    color_add_scheme($value['theme'], $value['hex'], $value['last_id'], $value['name']);
  }
  
  drupal_set_message("Added schemes for <em>{$theme}</em> to color schemes table.", "status");
}

/**
 * Retrieve the color info for a theme.
 *
 * @param $theme
 *   A string for the theme name
 * @param $source
 *   An optional string. 'sql' or 'inc'.
 *   Typically 'inc' is used for generated scheme before DB is populated for schemes
 *   Typically 'sql' is for current, generated schemes
 * @param $columns
 *   An optional array or string of what columns to return. If none, will return all rows.
 *   Currently not fully implemented, will default to all rows.
 * @return
 *   Array of theme info. Referred to in other functions as $info.
 */
function color_get_theme_info($theme, $source = "sql", $columns = NULL) {
  static $static;
  
  module_load_include('inc', 'color', 'color.misc');  
  
  // Cache results for these parameters.
  if (isset($columns)) {
    $select = (is_array($columns)) ? implode(', ', $columns) : $columns;
  }
  else {
    $select = '*';
  }
  
  if (isset($static[$theme][$source][$select])) {
    return $static[$theme][$source][$select];
  }
  
  if ($source == "inc") {
    $path = drupal_get_path('theme', $theme);
    $file = $path .'/color/color.inc';
    if ($path && file_exists($file)) {
      include $file;
    }
    
    // For flexibility, we're allowing certain schemes to work without color replacement and rely on base changing. Can be removed. Designed to work specially for 'shift' mode where only one scheme (the reference) needs all fields, the rest just need a base.
    foreach ($info['schemes'] as $key => $v) {      
      $fieldcount = count($info['fields']);
      $commas = substr_count($v, ',');
      if ($commas < $fieldcount - 1) {
        $commastoadd = ($fieldcount - 1) - $commas;
        for ($count = 0; $count < $commastoadd; $count += 1) {
          $v .= ",";
        } 
      }
      $info['schemes'][$key]  = explode(',', $v);
    }
    // End flexi-code
  }
  elseif ($source == "sql") {
    if (isset($theme)) {
      $info['theme'] = $theme;
    }
    
    // Get information regarding theme
    $result = db_query("SELECT %s FROM {color} WHERE theme = '%s'", $select, $theme);
    while ($row = db_fetch_array($result)) {
      
      if ($row['name'] == 'default_scheme') {
        $info[$row['name']] = $row['value'];
      }
      else {
        $info[$row['name']] = unserialize($row['value']);
      }
    }
    
    // Get color schemes for theme
    $result = db_query("SELECT cs.id, cs.name, cs.hex FROM {color_schemes} cs WHERE theme = '%s'", $theme);
    while ($row = db_fetch_array($result)) {
      // Get color schemes
      $info['schemes'][$row['name']] = unserialize($row['hex']);
      $info['scheme_ids'][$row['name']] = $row['id'];
    }
    ksort($info['schemes']);
  }

  $info['theme'] = $theme;
  $static[$theme][$source][$select] = $info;
  return $info;
}

/**
 * Return scheme information via UID and theme
 *
 * @param $uid
 *   Optional drupal user ID. UID 0 is unregistered user.
 * @return
 *   ARRAY. Scheme information.
 */
function color_find_scheme_by_UID($uid = 0, $theme, $columns = NULL) {

  // Cache results for these parameters.
  if (isset($columns)) {
    $select = (is_array($columns)) ? implode(', ', $columns) : $columns;
  }
  else {
    $select = '*';
  }
  
  if (isset($static[$uid][$theme][$select])) {
    return $static[$uid][$theme][$select];
  }
  
  if ($uid == 0) { // Unregistered users
    if (isset($_SESSION['scheme_id'])) {
      $scheme_id = $_SESSION['scheme_id'];
    }
    else {
      $scheme_id = 0;
    }

  }
  elseif ($uid != 0) { // Registered users
    if ($scheme = color_get_scheme('uid', $uid)) {
      $scheme_id = $scheme['id'];
    }
    else {
      $scheme_id = 0;
    }
  }
  
  // $scheme_id 0 is default site scheme for theme
  if ($scheme_id == 0 || !color_scheme_id_valid($scheme_id, $theme)) {
    $scheme = color_get_scheme('theme', $theme);
    $page_scheme_id = $scheme['id'];
  }
  else {
    $page_scheme_id = $scheme_id;
  }

  $scheme = color_get_scheme('scheme_id', $page_scheme_id);
  $scheme['id'] = $scheme_id;
  $scheme['page_id'] = $page_scheme_id;

  $static[$uid][$theme][$select] = $scheme;
  return $static[$uid][$theme][$select];
}

/**
 * Delete theme schemes, in DB and in scheme files.
 *
 * @param $theme
 *   String of theme name
 * @param $regen
 *   Optional boolean, if you do not want to regenerate schemes.
 */
function color_store_reset($theme, $regen = TRUE) {
  module_load_include('inc', 'color', 'color.misc');

  $info = color_get_theme_info($theme, 'inc');
  if (!color_validate($info)) {
    return;
  }
  
  db_query("DELETE FROM {color} WHERE theme = '%s'", $theme);
  db_query("DELETE FROM {color_schemes} WHERE theme = '%s'", $theme);
  db_query("DELETE FROM {variable} WHERE name LIKE 'color_%s%'", $theme);
  
  // Check color_users table. Clean out picks that don't match schemes.
  $result = db_query("SELECT scheme_id FROM {color_users}", $theme);
  while ($row = db_fetch_array($result)) {
    $scheme = color_get_scheme('scheme_id', $row['scheme_id'], 'theme');
    if ($scheme['theme'] != $theme) {
      db_query("DELETE FROM {color_users} WHERE scheme_id = '%s'", $row['scheme_id']);
    }
  }

  module_load_include('inc', 'color', 'color.misc');
  color_rm_dir(file_directory_path() .'/color', $theme);
  drupal_set_message(t('Colors Reset.'), 'status');
  
  if ($regen == TRUE) {
    color_check_themes($theme); 
  }
}

/**
 * Retrieve the reference scheme palette for a particular theme.
 *
 * @param $theme
 *   String of theme name
 * @param $info
 *   Optional array of theme info (usually OK to leave this blank)
 * @return
 *   Array of reference palette. Field names as keys, with corresponding hex code.
 */
function color_get_reference($theme, $info = NULL) {
  static $palette = array();
  if (isset($palette[$which][$theme])) {
    return $palette[$which][$theme];
  }

  if (isset($info)) {
    $info = color_get_theme_info($theme);
  }

  $scheme_name = ($scheme = color_get_scheme('scheme_id', $info['reference_scheme'], 'name')) ? $scheme['name'] : NULL;
    
  $palette[$theme] = array_combine($info['fields'], $info['reference_scheme']);

  return $palette[$theme];
}