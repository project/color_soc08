<?php
/**
 * @file
 * Provides image manipulation functions
 */
 
/**
 * Generate images
 *
 * @param $theme
 *   String with theme name
 * @param $info
 *   Array with theme info
 * @param $paths
 *   Array containing file paths to files
 * @param $palette
 *   Array of hex values. i.e. array('#ffffff', '#000000')
 * @return
 *   Array of generated image file paths
 */
function _color_render_images($theme, &$info, &$paths, $palette) {
  module_load_include('inc', 'color', 'color.algorithms');
  module_load_include('inc', 'color', 'color.database');

  $info = color_get_theme_info($theme);

  foreach ($info['img'] as $img) {
    // Prepare template image.
    $source = $paths['source'] .'/'. $img['file'];
    $source = imagecreatefrompng($source);

    imagealphablending($source, FALSE);
    imagesavealpha($source, TRUE);

    $width = imagesx($source);
    $height = imagesy($source);

    // Prepare target buffer.
    $target = imagecreatetruecolor($width, $height);
    imagealphablending($target, TRUE);

    // Fill regions of solid color.
    if (isset($img['fill']) && count($img['fill']) > 0) {
      foreach ($img['fill'] as $fill) {
        imagefilledrectangle($target, $fill[0], $fill[1], $fill[0] + $fill[2], $fill[1] + $fill[3], _color_gd($target, $palette[$fill[4]]));
      }
    }

    // Render gradient.
    if (isset($img['gradient']) && count($img['gradient']) > 0) {
      foreach ($img['gradient'] as $gradient) {
        for ($y = 0; $y < $gradient[3]; ++$y) {
          $color = _color_blend($target, $palette[$gradient[4]], $palette[$gradient[5]], $y / ($gradient[3] - 1));
          imagefilledrectangle($target, $gradient[0], $gradient[1] + $y, $gradient[0] + $gradient[2], $gradient[1] + $y + 1, $color);
        }
      }
    }
    // Blend over template.
    imagecopy($target, $source, 0, 0, 0, 0, $width, $height);

    // Clean up template image.
    imagedestroy($source);

    // Cut out slices.
    foreach ($img['slices'] as $file => $coord) {
      list($x, $y, $width, $height) = $coord;
      $base = basename($file);
      $image = $paths['target'] . $base;

      // Cut out slice.
      if ($file == 'screenshot.png') {
        $slice = imagecreatetruecolor(150, 90);
        imagecopyresampled($slice, $target, 0, 0, $x, $y, 150, 90, $width, $height);
        variable_set('color_'. $theme .'_screenshot', $image);
      }
      else {
        $slice = imagecreatetruecolor($width, $height);

        imagealphablending($target, TRUE);
        imagecopy($slice, $target, 0, 0, $x, $y, $width, $height);
      }

      // Save image.
      if (isset($img['transparent_color']) && !empty($img['transparent_color'])) {
        imagecolortransparent($slice, $black); 
      }
      imagepng($slice, $image);
      imagedestroy($slice);
      $paths['files'][] = $image;

      // Set standard file permissions for webserver-generated files
      @chmod(realpath($image), 0664);

      // Build before/after map of image paths.
      $paths['map'][$file] = $base;
    }

    // Clean up target buffer.
    imagedestroy($target);
  }
  
  return $paths;
}