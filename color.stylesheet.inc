<?php
/**
 * @file
 * Stylesheet modification functions
 */

/**
 * Rewrite the stylesheet to match the colors in the palette.
 *
 * @param $theme
 *  String of theme name
 * @param &$info
 *   Reference array of theme info
 * @param &$paths
 *   Reference array of file paths for CSS/image slices.
 * @param $palette
 *  Array of hexes
 * @param $style
 *  String of the stylesheet contents
 */
function _color_rewrite_stylesheet($theme, &$info, &$paths, $palette, $style) {
  module_load_include('module', 'color', 'color');
  module_load_include('inc', 'color', 'color.database');
  module_load_include('inc', 'color', 'color.misc');

  $info = color_get_theme_info($theme, "sql");

  // Split off the "Don't touch" section of the stylesheet.
  $split = "Color Module: Don't touch";
  if (strpos($style, $split) !== FALSE) {
    list($style, $fixed) = explode($split, $style);
  }
  
  foreach (array_keys($info['engine']) as $engine) {
    module_load_include('inc', 'color', 'engines/' . $engine);
    if (function_exists('_color_engine_' . $engine) == TRUE) {
      $style = call_user_func('_color_engine_'.$engine, $style, $info, $palette, $info['engine'][$engine]);
    }
  }
  /*
  if (array_key_exists('shift', $info['engine']) !== FALSE) {
    module_load_include('inc', 'color', 'engines/shift');
    $style = _color_engine_shift($style, $info, $palette, $info['engine']['shift']);
  }

  if (array_key_exists('tag', $info['engine']) !== FALSE) {
    module_load_include('inc', 'color', 'engines/tag');
    $style = _color_engine_tag($style, $info, $palette, $info['engine']['tag']);
  }
    */
  // Append fixed colors segment.
  if (isset($fixed)) {
    $style .= $fixed;
  }
    
  // Replace paths to images.
  foreach ($paths['map'] as $before => $after) {
    $before = base_path() . $paths['source'] . $before;
    $before = preg_replace('`(^|/)(?!../)([^/]+)/../`', '$1', $before);
    $style = str_replace($before, $after, $style);
  }

  return $style;
}



/**
 * Save the rewritten stylesheet to disk.
 */
function _color_save_stylesheet($file, $style, &$paths) {
  // Write new stylesheet.
  file_save_data($style, $file, FILE_EXISTS_REPLACE);
  $paths['files'][] = $file;

  // Set standard file permissions for webserver-generated files.
  @chmod($file, 0664);
}