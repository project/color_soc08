<?php
/**
 * @file
 * Misc functions.
 */

/**
 * Tests individual theme color.module support
 *
 * @param $theme
 *  String of theme name
 */
function color_compatible($theme) {
  static $static;

  $path = drupal_get_path('theme', $theme);
  $file = $path .'/color/color.inc';
  if ($path && file_exists($file)) {
      $static[$theme] = TRUE;
  }
  else {
      $static[$theme] = FALSE;
  }
  
  return $static[$theme];
}

/**
 * Return a list of colorable themes
 *
 * @return
*   Array of themes that support color.module
 */
function color_list_colorable_themes() {
  $lthemes = list_themes();
  foreach ($lthemes as $theme) {
    if (color_compatible($theme->name)) {
      $themes[] = $theme->name;
    }
  }
  return $themes;
}

/**
 * Recursively remove directly and file contents at location by directory
 *
 * @param $directory
 *  String of directory path
 * @param $theme
 *  Optional string for theme name
 */
function color_rm_dir($directory, $theme = NULL) {           
  if (is_dir($directory)) {
    $directory = (substr($directory, -1) != "/") ? $directory."/" : $directory;
    $open_dir = opendir($directory);

    while ($file = readdir($open_dir)) {
      if (!in_array($file, array(".", "..")) && (!$theme || ($theme && strstr($directory.$file, $theme)))) {
        if (!is_dir($directory.$file))
          @unlink($directory.$file);
        else
          color_rm_dir($directory.$file, $theme);
      }
    }
    closedir($open_dir);
    @rmdir($directory);
  }
}

/**
 * Normalize theme names
 *
 * @param $string
 *   String to normalize
 * @return
 *   Normalizing string.
 */
function color_normalize($string) {
   $string = preg_replace('~[^a-z0-9._]+~', '-', drupal_strtolower($string));
   return $string;
}

/**
 * Check the color.inc file for theme
 *
 * @param $info
 *   Array of theme info from color.inc. Usually taken from color_get_theme_info($theme, 'inc')
 * @return
 *   TRUE if theme validates. FALSE if not. Also output drupal error/status messages.
 */
function color_validate($info) {
  $validated = TRUE;
  $theme = $info['theme'];
  
  // Is 'options' set?
  if (isset($info['engine'])) {
    // Are any of the two engine options set?
    if ((array_key_exists('shift', $info['engine']) === FALSE) && (array_key_exists('tag', $info['engine']) === FALSE)) {
      drupal_set_message("(".$theme.") info[engine]: missing 'shift' and/or 'tag' option.", 'error');
      $validated = FALSE;
    }
    
    
    // Catch common color.inc error. Not referencing 'shift' mode when base/link/text option stated.
    if (isset($info['engine']['shift']) && array_search('base', $info['engine']['shift']) !== FALSE && array_search('base-only', $info['engine']['shift']) !== FALSE) {
      drupal_set_message("(".$theme.") info[engine][shift]: missing 'base' or 'base-only' option");
    }
  }
  else {
    drupal_set_message("(".$theme.") info[engine]: missing.", 'error');
    $validated = FALSE;
  }
  
  if (isset($info['fields'])) {
    $fieldcount = count($info['fields']);
  }
  else {
    drupal_set_message("(".$theme.") info[fields]: missing.", 'error');
    $validated = FALSE;
  }

  if (isset($info['img'])) {
    if (isset($info['blend_target'])) {
      if (strlen($info['blend_target']) != 7) {
        drupal_set_message("(".$theme.") info[blend_target]: (".$info['blend_target'].") is not a valid full hex (i.e. #ffffff).", 'error');
        $validated = FALSE;
      }
    }
    else {
        drupal_set_message("(".$theme.") info[blend_target]: missing.", 'error');
        $validated = FALSE;
    }
  }
  
  if (isset($info['default_scheme'])) {
    if (array_search($info['default_scheme'], array_keys($info['schemes'])) === FALSE) {
      $validated = FALSE;
      drupal_set_message("(".$theme.") info[default_scheme] (".$info['default_scheme']."): does not match a scheme name.", 'error');
    }
  }
  else {
      drupal_set_message("(".$theme.") info[default_scheme]: missing.", 'error');
      $validated = FALSE;
  }
  
  if (isset($info['engine']['shift']) && (array_search('base', $info['engine']['shift']) !== FALSE || array_search('base-only', $info['engine']['shift']) !== FALSE)) {
    if (isset($info['reference_scheme'])) {
      if (array_search($info['reference_scheme'], array_keys($info['schemes'])) === FALSE) {
        $validated = FALSE;
        drupal_set_message("(".$theme.") info[reference_scheme] (".$info['reference_scheme']."): does not match a scheme name", 'error');
      }
    }
    else {
        drupal_set_message("(".$theme.") info[reference_scheme]: missing.", 'error');
        $validated = FALSE;
    }
  }
    
  if (isset($info['schemes'])) {
    if (isset($info['engine']['shift']) && array_search('base-only', $info['engine']['shift']) === FALSE) {
      foreach ($info['schemes'] as $key => $v) {      
        $commas = count($v);
        foreach ($v as $num => $hex) {
          if (!isset($hex) || empty($hex)) {
            drupal_set_message("(".$theme.") info[schemes][{$key}]: missing color field {$num} is missing. (Color scheme fields error)", 'error');
            drupal_set_message("(".$theme.") (Color scheme fields error): It is possible a comma may be misplaced, leaving the colors out of order.", 'warning', FALSE);
            $validated = FALSE;

          }
          elseif (strlen($hex) != 7) {
            drupal_set_message("(".$theme.") info[schemes][{$key}]: color field {$num} ({$hex}) is not a full hex (i.e. #ffffff).", 'error');
            $validated = FALSE;
          }
        }
      }
    }
  }
  elseif (!isset($info['schemes'])) {
      drupal_set_message("(".$theme.") info[schemes]: missing.", 'error');
      $validated = FALSE;
  }
  else {

  }
  
  if (isset($info['css'])) {
    foreach ($info['css'] as $val) {    
      // Check for file
      if (!isset($val)) {
        $validated = FALSE;
        drupal_set_message("(".$theme.") info[css][{$val}]: No File provided for image!", 'error');
      }
      else {
        $path = drupal_get_path('theme', $theme) .'/';
        $file = $path . $val;
        if ($path && !file_exists($file)) {
          $validated = FALSE;
          drupal_set_message("(".$theme.") info[css][{$val}]: File ({$file}) does not exist.", 'error');
        }
      }
    }
  }
  
  if (isset($info['img'])) {
    // Move through $info['img'] array
    foreach ($info['img'] as $key => $val) {
      
      // Check for file
      if (!isset($val['file'])) {
        $validated = FALSE;
        drupal_set_message("(".$theme.") info[img][{$key}][file] - No File provided for image!", 'error');
      }
      else {
        $path = drupal_get_path('theme', $theme) .'/';
        $file = $path . $val['file'];
        if ($path && !file_exists($file)) {
            $validated = FALSE;
            drupal_set_message("(".$theme.") info[img][{$key}][file] - File does not exist.", 'error');
        }
      }
      
      // Fill
      if (isset($val['fill'])) {
        foreach ($val['fill'] as $fill) {
          // Possible feature: check dimensions
          if (array_search($fill[4], $info['fields']) === FALSE) {
            $validated = FALSE;
            drupal_set_message("(".$theme.") Missing fill color.", 'error');
          }
        }
      }
      
      // Gradient
      if (isset($val['gradient'])) {
        foreach ($val['gradient'] as $gradient) {
          // Possible feature: check dimensions
          if (array_search($gradient[4], $info['fields']) === FALSE) {
            $validated = FALSE;
            drupal_set_message("(".$theme.") Missing the top color field.", 'error');
          }
          if (array_search($gradient[5], $info['fields']) === FALSE) {
            $validated = FALSE;
            drupal_set_message("(".$theme.") Missing the bottom color field", 'error');
          }
        }
      }
    }
  }
  return $validated;  
}