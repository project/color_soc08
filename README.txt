
  TOC -- Table of Contents
========================
  1.) Color.module '08
  2.) Known issues / Todo
  3.) Ideas / Possible features
  4.) Documentation
    4d.) Installation and usage
  5.) Change log


  1) Color.module '08
========================
Improving color.module to empower theme developers and webmasters.

    a. Mission
  ==============================
    For:
    Drupal Theme developers, a better system for developing colorable themes.
    Drupal Webmasters, a better system for customizing and choosing schemes.
    Drupal Site users, a system to select their scheme.
    
    Aims:
    - More simple
    - More powerful
    - More possibilities
    
  My original proposal: http://groups.drupal.org/node/10072
  
    b. Goal
  ==============================
  I want to see this module appearing in some form in Drupal 7 because of its use to themers. I am using the demo of this module on two of my sites, I can't go back to the old one. I've grown accustomed to these features.
  
  I really think good documentation is the golden ticket to make this project work.
  
  A few months after Drupal 7 goes gold, I want to see an icon with themes that have color-compatibility. So people know they'll have color choice with the theme.
  
    c. Realistic stuff
  ==============================  
  - I find it rather presumptuous to assume that this project will be acceptable for a core commit  immediately after SOC. I hope after code/theming documentation is done I can present a case to core team/drupal community to help it reach the high standards expected of a core module. 
  - Many current themes in the Drupal theme library, if modified, can support color.module.
  - A select few themes are more time consuming/complex to support the module than others.
  - Theming for color.module is *not* as hard as it looks. You'll see. :)

  2) Known issues / Todos
========================
    a.) Caveats
  ==============================
  - This is still an experimental module, some interface stuff may not be polished yet.
  - There is a well thought-out installation system in underway that's pretty user-friendly. :) Currently testers may have to install devel module to reinstall things.
    
  Bug fixes/Patches:
  http://drupal.org/project/issues/color_soc08?projects=257666&categories=bug
  
    b.) To do
  ==============================
  - Spruce interface up
  - Documentation
  - Implement the "suggested colors" feature
  - Installer (color.module will kind of act like a theme 'service')
  - Work on the CSS for example themes. Lots of work to do on these.
  
  3) Ideas
========================
  - Group/Role default color schemes
  - Allow random default scheme
  - Allow random scheme per pageview
  - SVG Support (It's possible, however I don't know what person who designs with SVG's at this time.)
  - Implement a * for default schemes. Allow to choose 'site-default' scheme in select. Have a field in color.inc for default scheme on install.
  - Ability to enter static, hex value in fills.
  - A color.inc validator -- to make sure your color file is compatible as possible.
  - Per color-field "Previous colors"
  - More developer extensions, perhaps a "popup" farbtastic picker for schemes. Currently theme devs have to pick colors manually with the default scheme being already selected if they want to see results.
  - Enable/disable option for color.module on a per-theme basis.
  
  Feature requests / Feature dev: 
  If you have any ideas for color module, feel free to open an issue at:
  http://drupal.org/project/issues/color_soc08?projects=257666&categories=feature
  
  4) Documentation
========================
    a. For theme developers
  ==============================
What use is a module without documentation? What use is a color module without colorable themes?

I think it is in the best interest of every drupal themer to visualize a theme made to allow different schemes. The structure of your theme will maintain the same, you are only changing colors.  In the most complex case, you are filling the background of transparent PNG's with with solid/gradient colors. Color.module will update the CSS files for the images automatically.

    a. Quick-start (Installation and Usage)
  ==============================
Switch back to Garland. Backup your current /themes and /modules/color folders. Move them elsewhere temporarily.

Then themes from the /examples folder to your /themes. Install the new color.module in /color or /color_soc08. The module currently is in dev so the installation process may involve you having to disable/uninstall/reinstall to regenerate schemes for a theme. You can also regen themes via the theme's settings. Be sure to give the users the permission to choose schemes and give them the 'Pick color scheme' block if you want.

  5) Change log
========================
  August 22, 2008
  - Demonstration theme are being tweaked to look more presentable as colorable themes :)
  - Updated caching to include "Page compression"
  - New feature for theme developers, when scheme editing there will be a textfield where devs can have the color.inc code for a scheme to copy and paste right in! Yay! It was simple to do and helpful :)
  - Updated Nista theme. Updated its schemes. Very nice progress.
  - Updated Spooner, Cleanstate for text-decoration: none on anchors
  - Textarea resized for color.inc viewer
  - Color admin re-factored to conserve space.

  August 21, 2008
  - Fixed bug with color.js and Spooner (a base-only scheme)
  - Updated comments on engine/tag.inc
  - Updated this README a bit!

  August 20, 2008
    Most of these updates are fixes for the demonstration and documentation.
    The major exception is the stylesheet engine update.
  
  - Under the hood: Fixed bug with currenttheme + edit scheme button on color block sending to incorrect theme.
  - Under the hood: Removed superfluous data from edit_scheme form and submit
  - Under the hood: Changed permissions around. Color scheme is now available to those with user_access("administer access control")
  - Under the hood: color_edit_scheme_form() uses $palette instead of $reference to main hue and shift correctly. However, I left a not in the comments to document how $reference and $palette acted with the picker.
  - Under the hood: color_validate() action to the color_get_theme_info section.
  - Under the hood: Put spaces around dot's (.) which concatenate
  - Interface: For intuitive purposes, switching the "select" and "edit" buttons in the color_block_form
  - Demo Theme -- Simpla: removed extra breadcrumb
  - Demo Theme -- Friends Electric: Added default logo
  - Demo Themes -- Fixed font sizes cleanstate and simpla
  - Demo Users: Including a code of theme's color.inc on theme settings pages so viewers can take a look.
  - Documentation. On demo themes: Added comments and spruced up the color.inc on the examples more.
  - Stylesheet changing has been modularized into extensible color replacement engines.
      - engines/shift.inc - for shift themes, with a 'base' color (Garland)
      - engines/tag.inc - for custom CSS replacement
    - In your theme's color.inc, you can use both these engines at once in either order.
    - Developers can also add custom color replacement algorithms by adding their engines which hook in automatically. Create a engines/yourengine.inc with the function _color_engine_yourengine($style, $info, $palette, $options = NULL). Return the modified $style markup. You can input options in theme schemes (color.inc file) by adding 'yourengine' => array(MYVARS/ARRAY/ETC) to the 'engine' settings. Go crazy. :)
    - The color.module DON'T TOUCH statement now separates from all engines/modification as expected. The DON'T TOUCH comment is for CSS markup in your stylesheet's you do not want to have colors modified on.
    - Of course, this means, no more mode-cruft. :)

August 18, 2008 (Firm pencil's down date)
  Last minute touchups + Code documentation + Demonstration themes

  - markup: fixed bug with extra </div> closing tag in scheme editing caused by preview 
  - block: fixed bug with block on anonymous user scheme block
  - block: new "edit" button for super-administrator (UID 1), for quick access to scheme
  - block: "select scheme" is now "select"
  - block: blocks will no longer show up if scheme info not generated
  - under the hood: color_find_scheme_by_UID() rewritten
  - under the hood''fill' now supports unlimited of the same color. markup changed to array(x, y, width, height, color_name)
  - under the hood: Uninstall and Regenerate will now remove color_theme_* rows in {variable}.
  - under the hood: fixed bug with incorrect scheme stylesheets being applied to new theme
  - under the hood: color_validate() check to see if color.inc markup is correct.
  - under the hood: reworked color_check_themes()
    - supports array of themes, single theme variable
    - more efficient algorithm to check
    - includes support for color_validate()
  - under the hood: color_theme_scheme_generated() and color_theme_info_generated() return information on if a theme's color has been generated or not
  - under the hood: functions moved
    - color_get_theme_info() is now in color.database.inc
    - color_find_scheme_by_UID() is now color.database.inc
    - color_store_reset() is now color.database.inc
    - color_get_ref() is now in color.database.inc. Renamed to color_get_reference()
    - color_validate() is now in color.misc.inc
    - Reordered functions in color.module
  - under the hood: tweaking
    - color_user() will not include module files if not needed
  - under the hood: fix theme that show when block view
  - under the hood: reworked the way logo replacement works. slice or copy a logo.png file for a logo, or mention no logo.png in color.inc and it will use your theme's default logo.png (so you can use it later).
  - scheme editor: "Previous Colors" is now color history
  - scheme editor: will now see scheme title. :)
  - scheme editor: will now see breadcrumb including scheme
  - scheme editor: "Previous colors" and "Suggested colors" are now in a fieldset with a legend
  - scheme editor: if no scheme preview CSS or HTML is provided, no preview will be shown
  - Code: Added doxygen reference to functions
  - Demonstration themes: Completely redone themes: cleanstate, friendselectric*, nista, simpla, splender, spooner. Upgraded to new color module: Garland, Minnelli. All are featuring full color support and can be used as examples. :)
  
  * Updated from old 4.7 version (with color support added) -- however this is still bug-prone. There is a width problem in IE7 and button background one in IE6. Just a proof of concept for now. :)

August 8, 2008
  (#132571) to DRUPAL-6--1
  - Webmasters can now rename schemes
  - In theme settings, Edit scheme selectbox will have the default site scheme selected by default.
  - removed stray console.log() in color.js (was preventing farbtastic from attaching)
  - Updated the right branch. Apparently the -dev releases haven't updated for a whole month because I was only committing to the HEAD branch.
  - The "previous colors" (which is our undo function) and suggested colors should be clickable now :)

August 5, 2008
  (Including Commit #131347)
  - Now using scheme_id instead of hex values across the module.
  - Now using an "Official Site Scheme" in the selection.
  - Base and hybrid schemes will now require a 'reference' scheme. 
    - color_get_palette() is now color_get_theme('ref')
    - There is now a color_get_theme('fields') for retrieving fields
  - New theme config option, 'default_scheme'
  - Scheme settings completely renovated.
    - Interface:
      - Add scheme fieldset
      - Default scheme fieldset selection
      - Edit scheme fieldset selection
      - Rebuild scheme button
    - Edit scheme:
      - Added initial support for last colors, suggested colors -- they are not attached to current focused fields yet.
      - 'Make default' if scheme isn't site default
      - Can delete scheme, and save.
      - Preview is collapsible.
  - Redoing scheme installation. We will install necessary scheme info to tables before generating them.
      1.) Populate scheme table with information for generation.
      2.) Generate schemes one by one
      3.) For each generated scheme, update row in question
  - Scheme fields are now placed in an array format.
  - You can now add schemes -- Remember to press the 'Add scheme' button.
  - Editing scheme now works
  - Updated the example files.
  
July 30, 2008
  More leaps

  - (Bug) Fixed gradient preview
  - (Bug) Color locking/hooking functionality.
  - README updated. We have a documentation page over at 
  - (Improvement - Work in progress) Color scheme UI overhaul in progress
    - Darken current color
    - Lighten current color
    - Select random color
  - (Improvement - Work in progress) Add/Delete custom, named color schemes from theme configuration page. W/ AJAX
  - (Improvement) - Schemes changed in theme configuration will now update images and values when submitted.
  - (Improvement) Image overhaul
    - Unlimited images
    - Unlimited gradients
    - Unlimited solid-color fills
  - (Improvement) Stylesheet color replacement - We will now offer a Base-shift (garland-type) mode, a Custom mode for just tags, and a Hybrid which is a mix of the two. :) There is an option to automatically pick your mode.
  - (Minor addition) In the Garland color.inc example, I added the "Majestic" scheme. Looks pretty nice.
  - (Correcting ambiguity) In the blocks section, the block will be named 'Pick color scheme'.
  - (Correcting ambiguity) 'Regenerate schemes' button. On the theme config, there will be a new button for this. Previously, you would have to click "Reset to defaults" -- which was confusing and also defaulted other settings.
  - (Correcting ambiguity) table change
    `color_picks` is now `color_users`
  - (Correcting ambiguity) function color_store_theme() to color_add_theme()
  - (Changed name, same function) color_set_scheme() is now color_set_user_scheme()
    Function for setting user schemes selections (Table: `color_picks`)
  - (New function) color_set_scheme() 
    Formerly, this was used for storing users' scheme selections. I am now changing this function to the setting of the schemes themselves (Table: `color_users`).

July 23rd, 2008

  Allow BASE only color change! YES
    Notes: If only base color is listed in a scheme, only commas will be listed.
  - Fixed: Fix scheme reset not showing schemes on next pageview
  - Fixed errors for themes without fields 'base', 'text', 'link'
  - Fixed errors for themes without $info['copy']
  - Fixed errors for theme without $info['preview_image'] on preview.inc
  - Updated README.txt
  
  Color.js
    - is being fixed up for the custom preview.inc setup. The preview.css will change ID's matching your fields inside a div#preview. :)
    - Is being set up to work with instances where certain schemes in a 'base' colorable layout doesn't have the rest/some of the fields setup.
    - I have worked on reattaching the selectbox behavior for webmasters who want to add/remove custom schemes. (Has been driving me crazy!)

July 9, 2008
  Performance tweaks!
  - color_get_schemes(), color_get_theme_info(), color_compatible(), color_get_palette() all use static variables.
  - removed the extra $themes = list_themes(); in _color_rewrite_styleshet()
  - moved code around in color_form_alter() to only retrieve themes and include algorithms when needed. (~40-50ms performance fix)
  - Only taking need columns (id, name, hex) from schemes in color_get_theme_info()
  - fixed support for garland/minnelli screenshot on /user/{uid}/edit
  - fixed admin/build/themes/settings/{theme} to correctly set theme name
  - Cleanstate: fixed up cleanstate a bit.
  - Fixed default variable_get('cache') variables to fit the "Disabled" mode in admin/settings/performance by default.
  - Fixed notice with Undefined indexes by using isset()
  
  Unfinished:
  - Custom preview.inc option. Currently, if you provide a preview.inc in the theme's /color folder, the default will be overridden.
  
  Bugs:
  - Without user_access('select different theme')) and select color scheme users can pick schemes w/ block but not user profile.
  
June 26, 2008
  - Bug fixes here and there. I had a list but laptop was left to sleep without saving... Doing best to keep tabs.
  - make some functions for the _submit user scheme pick updates
  - Fixed cleanstate theme to replace colors correctly
  - Cleanstate scheme now has new custom fields + colors!
  - "Reset to defaults" will now delete schemes for themes in files. It will also delete relevant user color picks. However it may change with:
  - Fixed system up for clearing scheme_id's that don't exist.
  - Removed color_legacy function. People will be able to change w/ proper documentation.
  - Fixed bug On scheme change from Garland->Minelli, scheme won't change/update
  - Fixed bug on scheme change to cleanstate scheme shows as aquamarine but "Your scheme does not match with your theme!" shows and color_pick row WHERE uid={uid} won't delete..
  - Fixed bug with {{base}} color not switching!
  - Fixed bug with switching back to default site theme (scheme will not del)
  
  TODO: don't pull misc/file info unless necessary (not on page load alone)
  TODO: Use some static functions to reduce loading the same data twice.
  TODO: On moving to default theme, scheme select will not reset, unless after reload.
  
June 20, 2008
  - admin/build/themes/settings/{theme} will now change default color scheme
  - Fixed link color with CSS changing on Garland.
  - Fixed color block would show up on non-color themes
  - When cache is on, color.module will not change schemes for anonymous users.
  - "Reset to defaults" on /admin/build/themes/settings/{theme} will now correctly reset color schemes for themes
  - Updated cleanstate color.inc file

June 19, 2008
  SQL Tweaking/New DB Functions
  
  - (Done) INNER/LEFT JOIN more!
  - (Done) allow color_get_theme_info and work on grabbing specific fields instead of all columns
  - (Done) change 'default_set' to 'default_scheme'. TRY TO USE ID!
  - (Done) in color_theme_select_submit() before hand use hidden field to include scheme_id
  - (Todo) Store info on what themes are colorable in {color} table
    
June 18, 2008
  Bug fix/Tweaking release
    
  - (Added) On uninstall, whipe old schemes from dir
  - (Added) Do a preg_replace('~[^a-z0-9._]+~, '-', drupal_strtolower($string)); to normalize file paths
  - (Fixed) On block administration page, the theme now changes.
  - (Fixed) Users can now change schemes w/o 'select different theme' perm.
  - (Fixed) Bug on garland/minnelli with invalid stylesheet recoloring
  - (Tweaked) Suppress repeat warnings in certain parts of scripts.
  - (Updated) Update minnelli color.inc to new version.

  Caching
    I do not see this module as being very cache-friendly. There will have to be some thinking when it comes to this.
    
  - Block cache: lags on select boxes on anonymous/registered users
  - Caching modes 'normal': interrupts scheme changing for anonymous users. works OK w/ logged in ones

June 16, 2008
  New color replacement methods!

  Changes:
    - Find and replace fields from color.inc inside stylesheets. /* {{base}} */ #ffffff /* {{/base}} */
    - Include a 'base' field if you want to change colors of non-tagged color values. (I.e. Garland)
    - You can keep the above stylesheets in the /color folder by doing: 'css' => array( 'color/style.css',), 
    - Fixed bug w/ blend_target storing in DB
    - Added some more logic for falling back to default themes/color theme changing
  
  Quirks:
    - In listthemes(), must check if theme is enabled in system! This shows extra color-enabled themes in user/{uid}/edit
    - When changing over a theme, the old scheme name may show in selectbox (the module will still correctly move to default scheme for newly selected theme, though)

June 11, 2008
  We got a color scheme block + anonymous color scheme selection + perms.

  Changes:
  - Fixed up webmaster theme selection
  - Users other the uid 1 can choose scheme.
  - Color scheme changing block. (YES.)
  - Anonymous users can now pick color schemes
  - Started applying user permissions to scheme selection. If user is not granted permission they will not be able to see scheme selection and default to default_set scheme for the theme.
  
  Need to know: You have to set the 'choose color scheme' permissions for the user type if they want to see the block/change schemes.

June 9, 2008
  Changes:
  
  - Legacy support code is now in the color_get_theme_info() function.
  - Updated color.image.inc for some legacy support for gradients without the color field values. (supposed to be listed like x, y, width, height, color1, color2)
  - Modularized code into color.database.inc, color.stylesheet.inc and color.misc.inc
  - Now 3 tables: 'color' for theme info, 'color_schemes' for individual name/theme/hex info, and 'color_picks' for user scheme choices.
  - In color.database.inc, there is now functions to find scheme based on UID, hex, and scheme/theme
  - color_legacy($info) in color.misc.inc function for checking/changing old scheme .inc files to be compatible with this.
  
  Known quirks:
  - Minnelli is having very odd color effects. (http://img338.imageshack.us/img338/8634/picture8no4.png) (With current color.scheme using legacy support code to fix).


June 8, 2008
  Users can now pick color schemes
  
  Changes:
    + Color schemes are now in 'color_schemes' table
    + Instead of every theme having one scheme stored, all schemes are stored for user selection.
    + Scheme file information stored in 'extra_attr' column
    + In preparation for user custom schemes, the 'scheme' column will attempt to match with the 'name' column of a pre-made theme. If no match, in the future will assume it is a custom user theme. (Generate graphics/stylesheet)
    + Users can now pick color schemes for themes on a user basis via user/{uid}/edit. (YES)
    
  Known Quirks:
   - Color schemes are installed when color.module is installed.
   - New individual color themes are added when you go to admin/build/themes/settings/{theme}
   - When schemes are first installed, esp. after the module installation w/ a few color themes, the set messages list is very long. Need to find a way to quiet that.
    

June 1, 2008
  We are on our way to allowing users to select their color schemes.
  
  Changes:
    - For the moment the inline color scheme selection has been removed from /user/{uid}/edit. Can be placed back at a later time if wanted (it is in 1.6 of color.module)
    - Fixed bug with color schemer showing on none Color.module themes.
    - (Second commit today) Fixed bug where color schemes would change across themes. Every chosen theme w/ a scheme should have it's own row.
    + Users can now store their theme selections (not creations yet) in the DB. It will insert and edit.
    + New special 'user_scheme' name. Currently stores serialized hexed codes for color schemes. Built to support custom schemes for users.
    + A form submit checking for when a theme and color scheme is chosen at the same time.
    + (Second commit today): Used coder.module to brush up a bit.

May 30, 2008
  This is a commit so my mentors (Konstantin and Dmitri) and anyone else who wants to can see what I've been working on. There is a lot of temporary code in this commit.
  
  Changes:
  + Inline color scheme select
  + Fixed schemer to work with more themes!
  + Schemer now has some (maybe temporary) support for legacy color.inc files. It will present a warning but still adds to the database all the same.
  + Split functions up into color.image.inc and color.algorithms.inc
  - Removed primary key for indexes atm. For now to change this please Uninstall/Reinstall module. It will suppress some duplicate errors from before.
  
  Known Quirks: I cannot get the values of the select boxes for color schemes on color-enabled themes to all submit seperately on /user/{uid}/edit. It seems as if the last select box overwrites all the previous ones. And the ['colorselectbox'] hex shows up all around the form output with dprint_r($form);

May 28, 2008
  Changes:
  + Added the "uid" column. Index in scheme for 'theme', 'uid' for the moment.
  + Added "Color-enabled" in description of themes with color support (color.inc files present)
  + Added 
  
  Known quirks:
  - I just found that current color.module in Drupal 6 may not be handling screenshot preview correctly.
  
  
May 26, 2008
  Changes:
  + Support for unlimited/custom amount of color fields. Fields atm must be declared in color.inc.
  + Color scheme keys changed to scheme name (was value/hexes). Implemented storage of schemes/other color theme variables in SQL.
  + Color.module install schema.
  + "Reset to defaults" can be used to reset the SQL (incase you wanted to try adding new fields/colors).
  + Gradient colors can now be chosen through field names of your choice.
  
  Known Quirks:
  - (Fixed) Blank page on module enable/disable, theme change submit/reset. Try reloading the URL w/o form POST.