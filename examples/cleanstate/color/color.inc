<?php

$info = array(
    'fields' => array('background', 'contentlink', 'sidebarlink', 'sidebarheader', 'sidebar', 'text'),
    
    'default_scheme' => 'Blue Lagoon',
    'reference_scheme' => 'Blue Lagoon',
  // Pre-defined color schemes. If 'fields' is undefined, the default is 5 colors
  'schemes' => array(
    'Ash' => '#464849,#2f416f,#C3C7D1,#5d6779,#0a0e14,#494949',
    'Aquamarine' => '#55c0e2,#082f36,#0E8DA3,#007e94,#023741,#696969',
    'Belgian Chocolate' => '#d5b048,#6c420e,#090500,#971702,#560d00,#ffffff',
    'Blue Lagoon' => '#0072b9,#027ac6,#2385c2,#5ab5ee,#246a4c,#ffffff',
    'Bluemarine' => '#3f3f3f,#336699,#6598cb,#6598cb,#1e4062,#000000',
    'Citrus Blast' => '#d0cb9a,#917803,#efde01,#e6fb2d,#828e15,#000000',
    'Cold Day' => '#0f005c,#434f8c,#4d91ff,#1a1575,#2c23c7,#000000',
    'Greenbeam' => '#c9c497,#0c7a00,#03961e,#7be000,#37590d,#ffffff',
    'Mediterrano' => '#ffe23d,#a9290a,#fc6d1d,#a30f42,#cf5917,#000000',
    'Mercury' => '#788597,#3f728d,#a9adbc,#d4d4d4,#212121,#707070',
    'Nocturnal' => '#5b5fa9,#5b5faa,#5F63B1,#9fa8d5,#0b166b,#494949',
    'Olivia' => '#7db323,#6a9915,#b5d52a,#7db323,#537718,#191a19',
    'Pink Plastic' => '#12020b,#1b1a13,#f391c6,#f41063,#970c3f,#ffffff',
    'Shiny Tomato' => '#D14545,#c70000,#a1443a,#f21107,#83110b,#ffffff',
    'Teal Top' => '#18583d,#1b5f42,#34775a,#52bf90,#1a563c,#2d2d2d',
  ),

  'copy' => array(
    'images/menu-collapsed.gif',
    'images/menu-expanded.gif',
    'images/menu-leaf.gif',
  ),

  'css' => array('color/style.css'),

  'preview' => array(
    'css' => 'color/preview.css',
  ),

  'blend_target' => '#ffffff'
);